import youtube_dl

def download_audio(song_url, file_name):
    outtmpl = '~/Downloads/'+file_name + '.%(ext)s'
    ydl_opts = {
        'format': 'bestaudio/best',
        'outtmpl': outtmpl,
        'postprocessors': [
            {'key': 'FFmpegExtractAudio','preferredcodec': 'mp3',
             'preferredquality': '192',
            },
            {'key': 'FFmpegMetadata'},
        ],
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        info_dict = ydl.extract_info(song_url, download=True)


def download_video(url, output):
    ydl_opts = {}
    ydl_opts['outtmpl'] = '~/Downloads/'+output
    ydl_opts['merge_output_format'] = 'mp4'
    ydl_opts['format'] = 'bestvideo[ext=mp4]+bestaudio'
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([url])
        result = ydl.extract_info(url, download=False)
        outfile = ydl.prepare_filename(result) + '.' + result['ext']
    return outfile
tmp=7
video_url='https://www.youtube.com/watch?v=i873gZMulwQ'
#download_video(video_url,'12')
download_audio(video_url,'kwa')