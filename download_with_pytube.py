from pytube import YouTube
type='vid' #aud or vid
url='https://www.youtube.com/watch?v=3GwjfUFyY6M'
yt=YouTube(url)
assert (type in ['aud','vid'])
all_streams=yt.streams
if type=='aud':
    audio_streams=yt.streams.filter(only_audio=True)
    print(audio_streams)
    stream=audio_streams.get_by_itag(140)
#first().download()
elif type=='vid':
    print(all_streams)
    # in order to have vid with audio
    # the stream should have a vcodec and a acodec
    stream=all_streams.get_by_itag(18)
print('downloading ...')
stream.download(output_path='/home/yitzhak/Downloads/')
print('finished download ...')
